variable "region" {
    description = "AWS region"
    type        = string
    default     = "us-east-2"
}

variable "instance_type" {
    description = "AWS EC2 instance type"
    type        = string
    default     = "t2.micro"
}

variable "vpc_cidr" {
    description = "Network by project"
    type        = string
    default     = "172.18.0.0/24"
}

variable "subnet_cidr" {
    description = "Subnet from vpc"
    type        = string
    default     = "172.18.0.0/26"
}

variable "internal_elastic_ip" {
    description = "Internal ip for elasticsearch"
    type        = list(string)
    default     = ["172.18.0.10"]
}

variable "internal_kibana_ip" {
    description = "Internal ip for elasticsearch"
    type        = list(string)
    default     = ["172.18.0.11"]
}

variable "kibana_http_port" {
    description     = "Kibana web"
    type            = number
    default         = 5601
}

variable "allow_traffic" {
    description     = "Allow traffic"
    type    = list(object({
        port        = number
        proto       = string
        cidr_blocks = list(string)
    }))
    default = [
        {
            # Find how use var in var
            port        = 5601
            proto       = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
        },
        {
            port        = 22
            proto       = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
        }
    ]
}

variable "default_tags" {
    description = "Default tags for all resources"
    type = map
    default = {
        Owner = "Vladislav Sh."
    }
}

