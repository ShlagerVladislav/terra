resource "aws_vpc" "vpc_proj1" {
    cidr_block              = var.vpc_cidr
    enable_dns_support      = true
    enable_dns_hostnames    = true
    tags = var.default_tags
}

resource "aws_internet_gateway" "igw-proj1" {
    vpc_id  = aws_vpc.vpc_proj1.id
    tags = var.default_tags
}

resource "aws_default_route_table" "internet_route" {

    default_route_table_id = data.aws_route_table.main_route_table.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.igw-proj1.id
    }
  
    tags = var.default_tags
}

resource "aws_subnet" "subnet_proj1" {
    vpc_id              = aws_vpc.vpc_proj1.id
    cidr_block          = var.subnet_cidr
    availability_zone   = element(data.aws_availability_zones.azs.names, 0)
    tags = var.default_tags
}

resource "aws_network_interface" "elastic_ip" {
    subnet_id       = aws_subnet.subnet_proj1.id
    private_ips     = var.internal_elastic_ip
    tags = var.default_tags
}

resource "aws_network_interface" "kibana_ip" {
    subnet_id       = aws_subnet.subnet_proj1.id
    private_ips     = var.internal_kibana_ip
    tags = var.default_tags
}

resource "aws_security_group" "proj1-sg" {
    name    = "proj1-sg"
    vpc_id  = aws_vpc.vpc_proj1.id
    
    dynamic "ingress" {
        for_each = var.allow_traffic
        content {
            from_port   = ingress.value["port"]
            to_port     = ingress.value["port"]
            protocol    = ingress.value["proto"]
            cidr_blocks = ingress.value["cidr_blocks"]
        }
    }

    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    tags = var.default_tags

}