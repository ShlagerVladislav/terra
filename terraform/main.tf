resource "aws_instance" "elastic" {

    ami                         = data.aws_ami.RedHat7_latest.id
    key_name                    = aws_key_pair.elk_key.key_name
    instance_type               = var.instance_type
    vpc_security_group_ids      = [aws_security_group.proj1-sg.id]
    associate_public_ip_address = true
    subnet_id                   = aws_subnet.subnet_proj1.id

    tags = var.default_tags
}

resource "aws_instance" "kibana" {

    ami                         = data.aws_ami.RedHat7_latest.id
    key_name                    = aws_key_pair.elk_key.key_name
    instance_type               = var.instance_type
    vpc_security_group_ids      = [aws_security_group.proj1-sg.id]
    associate_public_ip_address = true
    subnet_id                   = aws_subnet.subnet_proj1.id

    tags = var.default_tags

}

resource "aws_key_pair" "elk_key" {
  key_name   = "elk-key"
  public_key = file("~/.ssh/id_rsa.pub")
}