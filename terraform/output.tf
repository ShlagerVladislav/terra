output "internal_elasticsearch_ip" {
    value = aws_instance.elastic.private_ip
}

output "public_elasticsearch_ip" {
    value = aws_instance.elastic.public_ip
}

output "internal_kibana_ip" {
    value = aws_instance.kibana.private_ip
}

output "pubcic_kibana_ip" {
    value = aws_instance.kibana.public_ip
}