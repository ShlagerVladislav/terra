# Get latest verstion RHEL 7
data "aws_ami" "RedHat7_latest" {
    most_recent = true 
    owners      = ["309956199498"]

    filter {
        name    = "name"
        values  = ["RHEL-7.*"]
    }

    filter {
        name    = "architecture"
        values  = ["x86_64"]
    }

    filter {
        name    = "virtualization-type"
        values  = ["hvm"]
    }

}

data "aws_route_table" "main_route_table" {
  filter {
    name   = "association.main"
    values = ["true"]
  }
  filter {
    name   = "vpc-id"
    values = [aws_vpc.vpc_proj1.id]
  }
}

data "aws_availability_zones" "azs" {
  state = "available"
}